jQuery(document).ready(function($) {
	function buildBlogSummary($post){
		var output =	'<div class="container-wrapper"><div class="container"><div class="row"><div class="col-lg-4 col-sm-12 title-wrapper"><div class="title-wrap"><div class="categories">' + $post.cats_string + '</div><div class="date">' + $post.pretty_date + '</div></div></div><div class="col-lg-8 col-sm-12 content-wrapper"><a href="' + $post.permalink + '"><h3 class="blog-title">' + $post.post_title + '</h3></a></div><!--/title-wrap--></div></div></div></div>';
		return output; 
	}
	function loadMore( $id, $category_id, $date, $keyword ){
		$.ajax({
			url: ajax_pagination.ajaxurl,
			type: 'post',
			async:    true,
			cache:    false,
			dataType: 'json',			
			data: {
				action: 'ajax_pagination',
				params: {
					'paged': ajaxPage,
					'category_id': $category_id,
					'date' : $date,
					'keyword' : $keyword
				}
			},
			fail: function(result){
				alert(10);
			},
			success: function( result ) {
				console.log( 'gooo', result);

				if(result.posts.length > 0){
					$.each(result.posts, function(k,post){
						console.log('POST',post);
						var div = buildBlogSummary(post);
						$('#' + $id).find('.blog-container').append(div);
						console.log('div' + $id,div);
					})
				}
				else{
					$('#' + $id).find('#load_more').html('All Posts Are Loaded').addClass('inactive');
				}				
				window.ajaxPage++;
			}
		})		
	}
	if('undefined' != typeof window.blogModules){
		$.each(window.blogModules, function(a,b){
			console.log('blogm:' + a,b);
			var category = $('#' + b).find('#blog_category').find(":selected").data('term_id');
			loadMore(b,category);
		})
		$('#load_more').click(function(e){
			e.preventDefault();
			var id = $(e.target).data('id');
			loadMore(id);
		})
		$('a#blog_search').click(function(e){
			e.stopPropagation();
			e.preventDefault();
			$('.blog-container').html('');
			var id = $(e.target).data('id');
			var category = $('#' + id).find('#blog_category').find(":selected").data('term_id');
			var date = $('#' + id).find('#blog_date').find(":selected").data('date_stamp');
			var keyword = $('#' + id).find('#blog_keyword').val();
			window.ajaxPage = 1;
			loadMore(id, category, date, keyword);
		})

	}
})