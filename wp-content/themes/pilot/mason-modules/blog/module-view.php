<?php 
	global $args;  
	global $wp;
	$current_slug = add_query_arg( array(), $wp->request );
	$url = explode("/", $current_slug);
	$selected_category_slug = 0;
	if( array_key_exists(1, $url) ){
		$selected_category_slug = $url[1];
	}
$dateQueryParams = array(
    'offset'           => 0,
    'orderby'          => 'ASC',
    'order'            => 'DESC',
    'post_type'        => 'post',
    'post_status'      => 'publish',
    'suppress_filters' => true 
);
$posts_array = get_posts( $dateQueryParams );
$earliest_date = $posts_array[0]->post_date;
$earliestDateObj = new DateTime($earliest_date);
$earliestDateObj->modify('first day of -2 month 00:00:00');

$todayObj = new DateTime();
$todayObj->modify('first day of this month 00:00:00');
$todayOut = $todayObj->format('F d Y');
$diff = $earliestDateObj->diff($todayObj);
$months = $diff->m;
$dates = [];
while($months >= 1){
	$earliestDateObj->modify('first day of +1 month 00:00:00');
	$dateOut = $earliestDateObj->format('F Y');
	$dates[strtotime($dateOut)] = $dateOut;
	$months--;
}
$dates = array_reverse($dates,true);
?>
<div class="blog-controls-wrapper">
	<div class="container-fluid container-lg container-md container-sm ">
		<div class="row">
			<div class="col-lg-12 blog-controls">
				<?php if ( count($args['cats'])> 0 ) : ?>
					<div class="select">
						<select id="blog_category" class="select-text">
							<option>All Categories</option>
							<?php foreach( $args['cats'] as $cat) : 
								$selected = "";
								if( $selected_category_slug === $cat->slug){
									$selected = "selected";
								}
							?>
								<option data-term_id='<?php echo $cat->term_id; ?>' <?php echo $selected; ?> ><?php echo $cat->name; ?></option>
							<?php endforeach; ?>
						</select>
						<span class="select-highlight"></span>

					</div>
				<?php endif; ?>
				<div class="select">
					<select id="blog_date" class="select-text">
						<option data-date=''>All Time</option>
						<?php foreach( $dates as $stamp => $date) : ?>
							<option data-date_stamp='<?php echo $stamp; ?>'><?php echo $date; ?></option>
						<?php endforeach; ?>
					</select>
					<span class="select-highlight"></span>

				</div>
				<div class="site-input">
					<input type="text" placeholder="Keywords" id="blog_keyword">
				</div>

				<a class="site-button"  id="blog_search" data-id="<?php echo $args['id']; ?>">Search</a>
			</div>
		</div>
	</div>
</div>
<div class="blog-container-wrapper">
	<div class="blog-container">
		<?php if( !array_key_exists('blogs' , $args )) : $post = $args['blog']; ?>		
			<div class="col-lg-12">
				<div class="img-block-wrap">
					<img class="blog-image" src=" <?php echo $post->url; ?>">
				</div><!--img-block-wrap-->
				<div class="title-wrap">
					<div class="caption"><?php echo $args['caption']; ?></div>
					<h3 class="blog-title"><?php echo $post->post_title; ?></h3>
					<a class="atc-button" href="<?php echo $post->permalink; ?>">Read</a>
				</div><!--/title-wrap-->
			</div>
		<?php endif; ?>
	</div>
</div>
<?php if( array_key_exists('blogs' , $args )) : ?>		
<div class="load-container-wrapper">
	<div class="container-fluid">
		<div class="row load-container">
			<div class="col-lg-12">
				<a class='site-button-two' id='load_more' href='' data-id="<?php echo $args['id']; ?>">Load More</a>
			</div>
		</div>
	</div>
</div>
	<script>
	jQuery(document).ready(function($) {
			if( !window.ajaxPage ){
				window.ajaxPage = 1;
			}
			if( !window.blogModules ){
				window.blogModules = [];
			}
			window.blogModules.push('<?php echo $args['id']; ?>' );
		})
	</script>
<?php endif; ?>