<?php
	function build_blog_layout(){
		global $i;
		$args = [];
		if( 'list' == mason_get_sub_field('blog_block_list') ){
			$blogs = [];
			$postArgs = array(
			  'numberposts' => 10
			);
			 
			$latest_posts = get_posts( $postArgs );
			$args['blogs'] = $latest_posts;
			$args['cats'] = get_categories();
		}
		else{
			$args['caption'] = mason_get_sub_field('blog_block_caption');
			$post_id = mason_get_sub_field('blog_block_post');
			$blog = get_post($post_id);
			$blog->permalink = get_permalink($blog->ID);
			if( $nail = get_post_thumbnail_id($blog->ID) ){
				$thumb_url_array = wp_get_attachment_image_src($nail, 'medium', true);
				$blog->url = $thumb_url_array[0];
			}
			else{
				$blog->url = "";				
			}
			if(!$args['caption']){
				$date = strtotime( $blog->post_date );
				$args['caption'] = date( 'F j, Y', $date );
			}
			$args['blog'] = $blog;
		}
		$args['module_styles'] = [];
//		print_r($args);
		if(get_sub_field('blog_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('blog_block_margin-top');
		}
		if(get_sub_field('blog_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('blog_block_margin-bottom');
		}
		
		if( is_array( $args ) ){
			return $args;
		}
	}
	function get_all_news( $args = array() ){
		$news = array();
		$defaults = array(
			'posts_per_page' => 2,
			'paged' => '1'
		);
		$params = array_replace_recursive($defaults, $args);
		$category_id = "";
		if( array_key_exists('category_id', $params)){
			$category_id = $params['category_id'];
		}
		$args=array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => $params['posts_per_page'],
			'paged' => $params['paged'],
			'cat' => $params['category_id']
		);
		$date = "";
		if( array_key_exists('date', $params) && $params['date'] > 0 ){
			$date = $params['date'];
			$firstDateObj = new DateTime();
			$firstDateObj->setTimestamp($date);
			$firstDateOut = $firstDateObj->format('F d, Y');

			$firstDateObj->modify('last day of this month 00:00:00');
			$lastDateOut = $firstDateObj->format('F d, Y');

		}		
		if( array_key_exists('keyword', $params) && $params['keyword'] ){
			$args['s'] = $params['keyword'];
		}
		if( $date ){
			$args['date_query'] = array(
		        array(
		            'after'     => $firstDateOut,
		            'before'    => $lastDateOut,
		            'inclusive' => true,
		        ),
		    );
		}
//		print_r($args);
		$news_query = new WP_Query($args);
		$news = [];
		$categoriesObj = get_categories();
		$cats = [];
		foreach($categoriesObj as $cat){
			$cats[$cat->term_id] = [
				'name' => $cat->name,
				'slug' => $cat->slug
			];
		}
		if( $news_query->have_posts() ):
			foreach( $news_query->posts as $post ){
				$post->permalink = get_permalink($post->ID);
				if( $nail = get_post_thumbnail_id($post->ID) ){
					$thumb_url_array = wp_get_attachment_image_src($nail, 'medium', true);
					$post->url = $thumb_url_array[0];
				}
				else{
					$post->url = "";				
				}
				$post_catsArr = wp_get_post_categories( $post->ID ); 
				$post->post_cats = [];
				$post_cats_name_arr = [];
				foreach($post_catsArr as $post_cat_term_id){
					$post->post_cats[] = [
						'term_id'=> $post_cat_term_id,
						'name' => $cats[$post_cat_term_id]['name'],
						'slug' => $cats[$post_cat_term_id]['slug']
					];
					$post_cats_name_arr[] = $cats[$post_cat_term_id]['name'];
				}
				$post->cats_string = implode(",", $post_cats_name_arr);
				$date = strtotime( $post->post_date );
				$post->pretty_date = date( 'm.d.Y', $date );

				$news[] = $post;
			}
		endif;
		return $news;
	}
//add_action( 'wp_ajax_nopriv_blog', 'get_ajax_news' );
//add_action( 'wp_ajax_blog', 'get_ajax_news' );
function get_ajax_news(){
}

function blog_enqueue() {
wp_enqueue_script( 'ajax_pagination',  get_stylesheet_directory_uri() . '/mason-modules/blog/ajax.js', array( 'jquery' ), '1.0', true );
wp_localize_script( 'ajax_pagination', 'ajax_pagination', array(
	'ajaxurl' => admin_url( 'admin-ajax.php' )
));


//wp_enqueue_script( 'ajax-blog', get_template_directory_uri() . '/dest/js/ajax.js', array('jquery') );
   //   wp_localize_script( 'ajax-blog', 'ajax_blog_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
 }

 add_action( 'wp_enqueue_scripts', 'blog_enqueue' );
 add_action( 'wp_ajax_nopriv_ajax_pagination', 'my_ajax_pagination' );
add_action( 'wp_ajax_ajax_pagination', 'my_ajax_pagination' );

function my_ajax_pagination() {
	$params = $_REQUEST['params'];
	$posts_per_page = 10;
	if( array_key_exists('posts_per_page',$params) ){
		$posts_per_page = $params['posts_per_page'];
	}
	$category_id = "";
	if( array_key_exists('category_id', $params)){
		$category_id = $params['category_id'];
	}
	$date = "";
	if( array_key_exists('date', $params)){
		$date = $params['date'];
	}
	$keyword = "";
	if( array_key_exists('keyword', $params)){
		$keyword = $params['keyword'];
	}
	$param = array(
		'post_type' => 'post', 
		'posts_per_page' => $posts_per_page, 
		'paged' => $params['paged'], 
		'category_id' => $category_id,
		'date' => $date,
		'keyword' => $keyword
	);
	$news = get_all_news( $param );
	$result = json_decode(json_encode($news), true);
	$response['posts'] = $result;
	echo json_encode( $response );
	exit;
}	


// on save, add rewrite rule to include custom url for the categories
add_action('acf/save_post', 'acfblog_rewrite');
function acfblog_rewrite( $post_id ) {
    $values = get_fields( $post_id );
	if(array_key_exists('content_blocks',$values) ){
		foreach($values['content_blocks'] as $block){
			if(array_key_exists('acf_fc_layout', $block) && 'blog_block' == $block['acf_fc_layout']){
				if(array_key_exists('blog_block_category_eng', $block)){
					$post = get_post($post_id);
					$pagename = strtolower($post->post_title);
					$rewrite_rules = get_option('theme_rewrite_rules');
					if(is_array( $rewrite_rules ) ){
						unset($rewrite_rules[$pagename]);
					}
					else{
						$rewrite_rules = [];
					}
					if(is_array( $block['blog_block_category_eng']) && count($block['blog_block_category_eng']) && 'include' == $block['blog_block_category_eng'][0]) {
						$rewrite_rules[$pagename] = [
					        $pagename . '/([^/]*)/?$',
					        'index.php?pagename=' . $pagename,
					          'top'	
						];
					}
					update_option('theme_rewrite_rules', $rewrite_rules);
					flush_rewrite_rules();					
				}
			}
		}

	}
}
add_action( 'init', 'site_rewrites_init' );
function site_rewrites_init(){
	$rewrite_rules = get_option('theme_rewrite_rules');
	if(is_array($rewrite_rules)){
		foreach($rewrite_rules as $rule_array){
		    add_rewrite_rule(
				$rule_array[0],
				$rule_array[1],
				$rule_array[2]
			 );

		}
	}
	flush_rewrite_rules();
}

?>