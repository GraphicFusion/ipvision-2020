<?php
    global $args;
    $block_name = 'media_block_';   
    $block_id = $args['acf_incr'];
    unset($args['acf_incr']);
?>
<?php if( array_key_exists('slides', $args) && count($args['slides'])> 0 ) : ?>
    <div id="slick-slider-<?php echo $block_id; ?>" style="">
        <?php foreach ( $args['slides'] as $arg ) : $slide_id = $arg[$block_name . "slide_id"]; 
                ?>
            <?php if( isset( $arg[$block_name . 'bg_video_file_mp4'] ) || isset( $arg[$block_name . 'image']  ) ) : ?>
                <div class="img-block-wrap video" id="<?php echo $slide_id; ?>" 
                    <?php if(!@$arg[$block_name . 'bg_video_file_mp4'] && $arg[$block_name . 'image']) : ?>
                            style="background-image: url(<?php echo $arg[$block_name . 'image']['url']; ?>);"
                    <?php endif; ?>
                >
                    <?php if(@$arg[$block_name . 'bg_video_file_mp4']) :  ?>
                        <div class="media-video" style="position: absolute; z-index: -1; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 50% 50%; background-image: none;">
                            <video class="bg-vid" autoplay loop="" muted="muted"
                    <?php if($arg[$block_name . 'image']) : ?>
                        poster="<?php echo $arg[$block_name . 'image']['url']; ?>"
                    <?php endif; ?>

                            ><source src="<?php echo $arg[$block_name . 'bg_video_file_mp4']['url']; ?>" type="video/mp4"></video>
                        </div><!--/media-video-->
                    <?php endif; ?>
                        <div class="img-overlay"> 
                            <div class="filter" 
                            <?php if( @$arg[$block_name . 'overlay_color'] ) :
                                list($r, $g, $b) = sscanf($arg[$block_name . 'overlay_color'], "#%02x%02x%02x"); 
                                $outColor = $r . ", " . $g . "," . $b .",";
                            ?>
                            style="background-image:linear-gradient(to right, rgba(<?php echo $outColor; ?> <?php echo $arg[$block_name . 'start_opacity']; ?>) 0%, rgba(<?php echo $outColor; ?> 0) 100% );"
                            <?php endif; ?>
                            >
                            </div>

                            <div class="media-container" >
                                <div class="slide-content">
                                    <?php if( isset( $arg[$block_name . 'subtitle'] ) && $arg[$block_name . 'subtitle'] ) : ?>
                                        <div class="lower-content">
                                            <div class="container container-card container-lg">
                                                <div class="row">
                                                    <div class="col-xs-6 blue-bg-wrapper">
                                                        <div class="blue-bg">
                                                                <?php echo $arg[$block_name . 'subtitle']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                <?php if(  @$arg[$block_name . 'video_file_mp4'] ||  @$arg[$block_name . 'youtube'] ) :
                                                    $video = (@$arg[$block_name . 'youtube'] ? @$arg[$block_name . 'youtube'] : $arg[$block_name . 'video_file_mp4']['url'] )
                                                 ?>
                                                    <a class="video-btn " href="<?php echo $video; ?>">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                                            <path d="M20,0C9,0,0,9,0,20s9,20,20,20c11,0,20-9,20-20S31,0,20,0z M15.4,30V10l13.3,10L15.4,30z" />
                                                        </svg>
                                                    </a>
                                                <?php endif; ?>
                                                <?php if(  @$arg[$block_name . 'button'] ) : ?>

                                                    <a class="inde-btn" href="<?php echo $arg[$block_name . 'button']['url']; ?>"><?php echo $arg[$block_name . 'button']['title']; ?></a>
                                                <?php endif; ?>
                                                </div>                                        
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div><!--/media-container-->
                        </div><!--/img-overlay-->
                    </div><!--/img-block-wrap-->
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div><!--/slick-slider-->

    <script>
        jQuery(document).ready(function($){
//            $('.video-btn').magnificPopup({
  //        type: 'iframe'
    //  });
            var dots = false;
        <?php if(count($args['slides'])>0) : ?>
            $('#slick-slider-<?php echo $block_id; ?>').on('init', function(e, slick, direction){
                $('.slick-dots').wrap( "<div class='col-lg-12'></div>" );
                $('.slick-dots').closest('.col-lg-12').wrap( "<div class='row'></div>" );
                $('.slick-dots').closest('.row').wrap( "<div class='container'></div>" );
                $('.slick-dots').closest('.container').wrap( "<div class='slick-dots-wrapper'></div>" );
                $('.slick-dots-wrapper').wrap("<div class='slick-dots-div'></div>" );
//                removeMobileVideo();
            });

        <?php endif; ?>
        <?php if(count($args['slides'])>0) : ?>
          $('#slick-slider-<?php echo $block_id; ?>').slick({
            slidesToShow:1,
            dots:false,
            arrows:false,
            //adaptiveHeight: true
          });
        <?php endif; ?>

        });

      
    </script>
<?php endif; ?>