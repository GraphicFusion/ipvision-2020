<?php
	function build_media_layout( $params ){
		global $i;
		$incr = 1;

		$slides = mason_get_sub_field('media_block_slides');
		$args = [
			'slides' => []
		];
		$skip_forced = 0;
		$args['skip_header'] = 0;
		$args['module_classes']['height_class'] = mason_get_sub_field('media_block_height') ? 'media-tall' : 'media-short';
		if(is_array($slides) && count($slides)>0){
			foreach($slides as $slide){
				$slide['media_block_slide_id'] = 'media_block_slide_'.$i.'_#'.$incr;
				$incr++;

				$args['slides'][] = $slide;
			}
			$skip_forced = 1;
		}
		if(!$skip_forced && ($params['forced'] || is_single())){ // should check what cpt
			$type = get_post_type();
			if(get_field($type.'_disable_header') && $params['forced'] ){
				$args['skip_header'] = 1;
			}
			else{
				$img = get_field($type . '_image');
				$icon_obj = get_field($type . '_icon');
				$url = "";
				$title = "";
				if('person' != $type){
					$title = get_the_title();
				}
			    $args['slides'][] = [
			        'media_block_use_popup' => "",
			        'media_block_image' => $img,
			        'media_block_logo_or_title' => "title",
			        'media_block_title' => $title,
			        'media_block_slide_id' => "media_block_slide_0_#1",
			        'media_block_bg_video_file_mp4' => "",
			        'media_block_overlay_color' => "",
			        'media_block_overlay_opacity' => "",
			        'media_block_icon' => $icon_obj

			    ];
			}
		}
		$args['acf_incr'] = "_forced";
		$args['module_styles'] = [];
		if(get_sub_field('media_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('media_block_margin-top');
		}
		if(get_sub_field('media_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('media_block_margin-bottom');
		}


//		print_r($args);
		return $args;
/*
		$custom_classes = " " . mason_get_sub_field('media_block_custom_class');
		$button_object = ( mason_get_sub_field('media_block_button_href') ? mason_get_sub_field('media_block_button_href') : mason_get_sub_field('media_block_custom_button_href') );
		if( is_object( $button_object ) ){
			$button_href = get_permalink( $button_object->ID);
		}
		else{
			$button_href = $button_object;
		}
		$media_args = array(
			'subtitle' => mason_get_sub_field('media_block_subtitle'),
			'button_text' => mason_get_sub_field('media_block_button_text'),
			'button_href' => $button_href,
			'id' => 'media_block_'.$i,
			'overlay_color' => '',
			'overlay_opacity' => '',
		);
		$media_args['title'] = mason_get_sub_field('media_block_title');
		$media_args['logo'] = mason_get_sub_field('media_block_logo');
		if( mason_get_sub_field('media_block_modify') ){
			if( $opacity = mason_get_sub_field('media_block_overlay_opacity') ){
				$media_args['overlay_opacity'] = $opacity;
			}
			if( $color = mason_get_sub_field('media_block_overlay_color') ){
				$media_args['overlay_color'] = $color;
			}
		}				
		$image = mason_get_sub_field('media_block_image');
		if( is_array( $image ) ){
			$media_args['image_url'] = $image['url'];
		}
		$mp4_file = mason_get_sub_field('media_video_file_mp4');
		if( is_array( $mp4_file ) ){
			$media_args['width_class'] = $media_args['width_class'] . " video-media";
			$media_args['video_file_mp4'] = $mp4_file['url'];
			$media_args['fallback_image_url'] = $image['url'];
		}
		$args[] = $media_args;
		*/

	}

?>