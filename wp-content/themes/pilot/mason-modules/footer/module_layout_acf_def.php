<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => 'xeiwf83477fdfooter',
		'name' => 'footer_block',
		'label' => 'Footer',
		'display' => 'block',
		'sub_fields' => array (
			array(
				'key' => create_key('footer','logo'),
				'label' => 'Logo',
				'name' => 'footer_block_logo',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array(
				'key' => create_key('footer','copyright'),
				'label' => 'Copyright',
				'name' => 'footer_block_copyright',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
			),
			array(
				'key' => create_key('footer','lower_content'),
				'label' => 'Lower Generic Content',
				'name' => 'footer_block_lower_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
			),
		),
		'min' => '',
		'max' => '',
	);
?>