<?php
	function build_footer_layout(){
		$logo = "";

		if( $img = mason_get_sub_field('footer_block_logo') ){
			$logo = $img['url'];
		};
		$args = array(
			'logo' => $logo,
			'copyright' => mason_get_sub_field('footer_block_copyright'),
			'info' => mason_get_sub_field('footer_block_lower_content')
		);
		return $args;
	}
	function footer_enqueue() {
		wp_enqueue_script('footer_js',  get_stylesheet_directory_uri() . '/mason-modules/footer/module.js', array( 'jquery' ), '1.0', true );
	}
	add_action( 'wp_enqueue_scripts', 'footer_enqueue' );

?>