<?php
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="container-fluid">
	<div class="footer-content" style="width:100%; ">
		<!--<div class="menu-close-wrapper-close">
			<div class="menu-button-close">
				<img class="menu-close menu-blue" src="<?php echo get_template_directory_uri(); ?>/mason-modules/navigation/images/ICON-CLOSE-GRAY.svg">						
			</div>
		</div>-->
		<div class="row">
			<div class="col-lg-12 logo-wrapper">
				<a href='/'>
					<img class='logo' src='<?php echo $args['logo']; ?>'>
				</a>
			</div>
		</div>
		<div class="row" id="footer_navigation">
			<div class="col-lg-4" id="footer_info" style="color:white">
				<div class="nav-wrapper">
					<?php echo $args['info']; ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="copyright-container" style="">
					<?php echo $args['copyright']; ?>
				</div>
			</div>
		</div>	
	</div>	
</div>