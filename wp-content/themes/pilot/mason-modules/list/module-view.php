<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
	<?php
	if($args['items']) :
		$i = 0;
		$left = [];
		$right = [];
		foreach($args['items'] as $item) :
			$link = $item['item'];
			$list = '
				<div class="list-item">
					<img src="\wp-content\themes\pilot\mason-modules\list\img\Icon-Arrow-Right.svg">
					<h5 class="li-content"><a href="' . $link['url'] .'">'. $link['title'] .'</a></h5>
				</div>';
			if( !( $i % 2) ){
				$left[] = $list;
			}
			else{
				$right[] = $list;
			}

			$i++;
		endforeach;
	endif; 
?>
<div style="">
	<div class="container-fluid bg-<?php echo $args['background']; ?>" >
		<div class="row">
			<div class="col-lg-4">
				<?php if($args['title']) : ?>
					<div class="title"><?php echo $args['title']; ?></div>
					<?php if(array_key_exists('link',$args) && $args['link']) : ?>
						<a class="site-button-two" href="<?php echo $args['link']['url']; ?>"><?php echo $args['link']['title']; ?></a>
					<?php endif; ?>
				<?php endif; ?>
			</div>		
			<div class="col-lg-4">
				<div class="list-content">
					<?php echo implode('',$left); ?>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="list-content">
					<?php echo implode('',$right); ?>
				</div>
			</div>
		</div>
	</div>
</div>