<?php
	global $pilot;
	$name = 'list';
	
	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key($name,'block'),
		'name' => $name . '_block',
		'label' => 'List',
		'display' => 'block',
		'sub_fields' => array (
			array (
		        'key' => create_key($name,'title'),
				'label' => 'Left Column Content',
				'name' => $name . '_block_title',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '50%',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array (
				'key' => create_key($name,'link'),
				'label' => 'Left Column Link',
				'name' => $name . '_block_link',
				'type' => 'link',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '50%',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
			),
			array(
		        'key' => create_key($name,'items'),
				'label' => 'Links',
				'name' => $name . '_block_items',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => 'Add Link',
				'sub_fields' => array(
					array(
				        'key' => create_key($name,'item'),
						'label' => 'Link',
						'name' => 'item',
						'type' => 'link',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
				),
			),
		),
		'min' => '',
		'max' => '',
	);
?>