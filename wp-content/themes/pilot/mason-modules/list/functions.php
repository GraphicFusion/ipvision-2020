<?php
	function build_list_layout(){
		$name = 'list';
		$args = array(
			'title' => mason_get_sub_field($name . '_block_title'),
			'background' => mason_get_sub_field($name . '_block_background'),
			'link' => mason_get_sub_field($name . '_block_link'),
			'items' => mason_get_sub_field($name . '_block_items')
		);
		$args['module_styles'] = [];
		if(get_sub_field($name.'_block_background')){
			$args['module_styles']['background'] = get_sub_field($name.'_block_background');
		}

		return $args;
	}
?>