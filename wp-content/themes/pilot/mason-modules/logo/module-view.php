<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div style="">
	<div class="container-fluid <?php echo $args['background']; ?>" >
		<div class="row">
			<div class="col-lg-12">
				<?php if($args['title']) : ?>
					<div class="title"><?php echo $args['title']; ?></div>
				<?php endif; ?>
			</div>		
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="board">
					<?php if($args['items']) : $fourby_i = 1; $fourby_row =1; $threeby_row = 1; $threeby_i = 1; $twoby_i = 1; $twoby_row =1;?>
						<?php foreach($args['items'] as $item) : 
							if( $threeby_row ){ $threeby_class = "threeby_row"; }else{ $threeby_class = ""; }
							if( $fourby_row ){ $fourby_class = "fourby_row"; }else{ $fourby_class = ""; }
							if( $twoby_row ){ $twoby_class = "twoby_row"; }else{ $twoby_class = ""; }
						?>
							<div class="logo-wrapper twoby-<?php echo $twoby_i." ".$twoby_class; ?> fourby-<?php echo $fourby_i." " .$threeby_class." " .$fourby_class; ?> threeby-<?php echo $threeby_i; ?>">
								<div class="logo-content">
									<img src="<?php echo $item['logo_block_image']['sizes']['large']; ?>">
								</div>
							</div>
						<?php 
							if( ($fourby_i % 4) ){ 
								$fourby_i++;
							}
							else{
								$fourby_i = 1;
								$fourby_row = 0;
							}
							if( ($threeby_i % 3) ){ 
								$threeby_i++;
							}
							else{
								$threeby_i = 1;								
								$threeby_row = 0;
							}
							if( ($twoby_i % 2) ){ 
								$twoby_i++;
							}
							else{
								$twoby_i = 1;
								$twoby_row = 0;
							}
						 endforeach;  
						 ?>
						 <?php if( $fourby_i <= 4 && 1 != $fourby_i ) : ?>
						 	<?php while( $fourby_i <= 4 ) : ?>
								<div class="logo-wrapper logo-wrapper-empty fourby fourby-<?php echo $fourby_i; ?>">
									<div class="logo-content-empty"> 
									</div>
								</div>
						 	<?php $fourby_i++; endwhile; ?>
						 <?php endif; ?>
						 <?php if( $threeby_i <= 3 && 1 != $threeby_i ) : ?>
						 	<?php while( $threeby_i <= 3 ) : ?>
								<div class="logo-wrapper logo-wrapper-empty threeby threeby-<?php echo $threeby_i; ?>">
									<div class="logo-content-empty"> 
									</div>
								</div>
						 	<?php $threeby_i++; endwhile; ?>
						 <?php endif; ?>
						 <?php if( $twoby_i <= 2 && 1 != $twoby_i ) : ?>
						 	<?php while( $twoby_i <= 2 ) : ?>
								<div class="logo-wrapper logo-wrapper-empty twoby twoby-<?php echo $twoby_i; ?>">
									<div class="logo-content-empty"> 
									</div>
								</div>
						 	<?php $twoby_i++; endwhile; ?>
						 <?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>