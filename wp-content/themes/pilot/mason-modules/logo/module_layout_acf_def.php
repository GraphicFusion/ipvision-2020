<?php
	global $pilot;
	$name = 'logo';
	
	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key($name,'block'),
		'name' => $name . '_block',
		'label' => 'Logo Board',
		'display' => 'block',
		'sub_fields' => array (
			array (
		        'key' => create_key($name,'title'),
				'label' => 'Content',
				'name' => $name . '_block_title',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array(
		        'key' => create_key($name,'items'),
				'label' => 'Logos',
				'name' => $name . '_block_items',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => 'Add Logo',
				'sub_fields' => array(
		            array(
		                'key' => create_key($name,'image'),
		                'label' => 'Image',
		                'name' => $name .'_block_image',
		                'type' => 'image',
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => array(
		                ),
		                'wrapper' => array(
		                    'width' => '40%',
		                    'class' => '',
		                    'id' => '',
		                ),
		                'return_format' => 'array',
		                'preview_size' => 'thumbnail',
		                'library' => 'all',
		                'min_width' => '',
		                'min_height' => '',
		                'min_size' => '',
		                'max_width' => '',
		                'max_height' => '',
		                'max_size' => '',
		                'mime_types' => '',
		            ),
				),
			),
		),
		'min' => '',
		'max' => '',
	);
?>