<?php
	function build_logo_layout(){
		$name = 'logo';
		$args = array(
			'title' => mason_get_sub_field($name . '_block_title'),
			'background' => mason_get_sub_field($name . '_block_background'),
			'link' => mason_get_sub_field($name . '_block_link'),
			'items' => mason_get_sub_field($name . '_block_items')
		);
		$args['module_styles'] = [];
		if(get_sub_field($name . '_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field($name . '_block_margin-top');
		}
		if(get_sub_field($name . '_block_z-index')){
			$args['module_styles']['z-index'] = get_sub_field($name . '_block_z-index');
		}
		if(get_sub_field($name . '_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field($name . '_block_margin-bottom');
		}

		return $args;
	}
?>