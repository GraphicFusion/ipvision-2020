<?php
	function build_navigation_layout(){
		$args = array(
			'alert_text' => mason_get_sub_field('navigation_block_alert_text'),
			'alert_link' => mason_get_sub_field('navigation_block_alert_link'),
			'alert_active' => mason_get_sub_field('navigation_block_alert_visibility'),
			'phoneDisplay' => mason_get_sub_field('navigation_block_phoneDisplay'),
			'phone' => mason_get_sub_field('navigation_block_phone'),
			'logo' => mason_get_sub_field('navigation_block_logo'),
			'location' => mason_get_sub_field('navigation_block_location'),
			'main_links' => mason_get_sub_field('navigation_block_main_nav')
		);

		return $args;
	}
	function nav_enqueue() {
		wp_enqueue_script( 'nav_hide',  get_stylesheet_directory_uri() . '/mason-modules/navigation/module.js', array( 'jquery' ), '1.0', true );
 	}
	add_action( 'wp_enqueue_scripts', 'nav_enqueue' );
?>