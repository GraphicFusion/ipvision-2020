<?php
	global $pilot;
	$name = 'article';
    $args = array(
        'post_type' => 'post',
        'post_status' => array('publish'),
        'numberposts' => -1    
    );
    $query = new WP_Query($args);
//    print_r($query);
    $posts = [];
    if($query->posts){
        foreach($query->posts as $post){
            $posts[$post->ID] = $post->post_title;
        }
    }

	$cats = get_categories();
	$cat_list = [];
	foreach($cats as $cat){
		$cat_list[$cat->term_id] = $cat->name;
	}

	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key($name,'block'),
		'name' => $name . '_block',
		'label' => 'Featured Articles',
		'display' => 'block',
		'sub_fields' => array (
			array (
		        'key' => create_key($name,'title'),
				'label' => 'Left Column Content',
				'name' => $name . '_block_title',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '50%',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array (
				'key' => create_key($name,'link'),
				'label' => 'Left Column Link',
				'name' => $name . '_block_link',
				'type' => 'link',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '50%',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
			),
			array(
				'key' => create_key($name,'method'),
				'label' => 'Method',
				'name' => $name . '_block_method',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '50%',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'manual' => 'Manual',
					'latest' => 'Latest',
					'categories' => 'Categories',
				),
				'allow_null' => 0,
				'other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
				'return_format' => 'value',
				'save_other_choice' => 0,
			),
			array (
		        'key' => create_key($name,'number'),
				'label' => 'Number of Posts',
				'name' => $name . '_block_number',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => create_key($name,'method'),
							'operator' => '==',
							'value' => 'categories',
						),
					),
					array(
						array(
							'field' => create_key($name,'method'),
							'operator' => '==',
							'value' => 'latest',
						),
					),
				),
				'wrapper' => array (
					'width' => '50%',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array(
		        'key' => create_key($name,'categories'),
				'label' => 'Select Categories',
				'name' => $name . '_block_categories',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => create_key($name,'method'),
							'operator' => '==',
							'value' => 'categories',
						),
					),
				),
				'wrapper' => array(
					'width' => '50%',
					'class' => '',
					'id' => '',
				),
				'choices' => $cat_list,
				'default_value' => array(
				),
				'allow_null' => 0,
				'multiple' => 1,
				'ui' => 1,
				'ajax' => 0,
				'return_format' => 'value',
				'placeholder' => '',
			),
			array(
		        'key' => create_key($name,'items'),
				'label' => 'Articles',
				'name' => $name . '_block_items',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => create_key($name,'method'),
							'operator' => '==',
							'value' => 'manual',
						),
					),
				),
				'wrapper' => array(
					'width' => '50%',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => 'Add Article',
				'sub_fields' => array(
		            array(
		                'key' => create_key($name,'item'),
		                'label' => 'Featured Articles',
		                'name' => 'item',
		                'type' => 'select',
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => "",
		                'wrapper' => array(
		                    'width' => '',
		                    'class' => '',
		                    'id' => '',
		                ),
		                'choices' => $posts,                
		                'taxonomy' => '',
		                'allow_null' => 0,
		                'multiple' => 0,
		                'return_format' => 'object',
		                'ui' => 1,
		            ),            
				),
			),
		),
		'min' => '',
		'max' => '',
	);
?>