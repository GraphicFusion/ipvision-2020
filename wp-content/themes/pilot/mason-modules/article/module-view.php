<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
	<?php
	if($args['items']) :
		$i = 0;
		$left = [];
		$right = [];
		foreach($args['items'] as $article) :
			$article = '
				<div class="article-item">
					<div class="article-meta">
						<div class="categories">'. $article->categories .'</div>
						<div class="date">' . $article->pretty_date .'</div>
					</div>
					<h5 class="li-content"><a href="'. $article->permalink . '">' . $article->post_title .'</a></h5>
				</div>';
			if( !( $i % 2) ){
				$left[] = $article;
			}
			else{
				$right[] = $article;
			}

			$i++;
		endforeach;
	endif; 
?>
<div style="">
	<div class="container-fluid" >
		<div class="row">
			<div class="col-lg-4">
				<?php if($args['title']) : ?>
					<div class="title"><?php echo $args['title']; ?></div>
					<?php if(array_key_exists('link',$args) && $args['link']) : ?>
						<a class="site-button-two" href="<?php echo $args['link']['url']; ?>"><?php echo $args['link']['title']; ?></a>
					<?php endif; ?>
				<?php endif; ?>
			</div>		
			<div class="col-lg-4">
				<div class="article-content">
					<?php echo implode('',$left); ?>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="article-content">
					<?php echo implode('',$right); ?>
				</div>
			</div>
		</div>
	</div>
</div>