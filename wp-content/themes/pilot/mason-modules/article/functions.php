<?php
	function build_article_layout(){
		$name = 'article';
		$method = mason_get_sub_field($name . '_block_method');
		$args = array(
			'title' => mason_get_sub_field($name . '_block_title'),
			'link' => mason_get_sub_field($name . '_block_link'),
			'items' => []
		);
		$posts_per_page = 6;
		$articles = [];
		$number = mason_get_sub_field($name . '_block_number');
		if( 'categories' == $method){
			$categories = mason_get_sub_field($name . '_block_categories');
			$query_args=array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'posts_per_page' => $number,
				'category__in' => $categories
			);
			$article_query = new WP_Query($query_args);
			if($article_query->have_posts() ){
				$articles = $article_query->posts;
			}
		}
		if( 'latest' == $method){
			$query_args=array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'posts_per_page' => $number
			);
			$article_query = new WP_Query($query_args);
			if($article_query->have_posts() ){
				$articles = $article_query->posts;
			}
		}
		if( 'manual' == $method){
			$items = mason_get_sub_field($name . '_block_items');
			if(count($items)>0){
				foreach($items as $arr){
					$articles[] = get_post($arr['item']);
				}
			}
		}
		$cats = get_categories();
		$cat_list = [];
		foreach($cats as $cat){
			$cat_list[$cat->term_id] = $cat->name;
		}
		if(count($articles)>0){
			foreach($articles as $blog){
				$blog->permalink = get_permalink($blog->ID);
				$date = strtotime( $blog->post_date );
				$blog->pretty_date = date( 'm.d.Y', $date );
				$blog_categories = wp_get_post_categories($blog->ID);
				$cat_array = [];
				foreach($blog_categories as $cat_id){
					$cat_array[] = $cat_list[$cat_id];
				}
				$blog->categories = implode(',', $cat_array);
				$args['items'][] = $blog;
			}
		}
		$args['module_styles'] = [];
		if(get_sub_field($name.'_block_background')){
			$args['module_styles']['background'] = get_sub_field($name.'_block_background');
		}
		if(get_sub_field($name . '_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field($name . '_block_margin-top');
		}
		if(get_sub_field($name . '_block_z-index')){
			$args['module_styles']['z-index'] = get_sub_field($name . '_block_z-index');
		}
		if(get_sub_field($name . '_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field($name . '_block_margin-bottom');
		}
		return $args;
	}
?>