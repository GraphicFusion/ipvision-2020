<?php
	function get_styling_fields( $module ){
		$included_modules = [
			'halfhalf',
			'generic_content',
			'article',
			'list'
		];
		if(in_array($module,$included_modules)){
			$module = $module . "_block_";
			$fields =  array(
				/*
				array(
					'key' => create_key($module,'module_styling'),
					'label' => 'Styling Parameters',
					'name' => $module.'_module_styling',
					'type' => 'true_false',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '50%',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => 'Hide Styles',
					'ui_off_text' => 'Show Styles',
				),
				
				array(
					'key' => create_key($module,'top_padding'),
					'label' => 'Top Padding',
					'name' => $module.'padding-top',
					'type' => 'text',
					'instructions' => 'Add padding to Module as: "10%" or "10px"',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array(
						'width' => '30%',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => create_key($module,'bottom_padding'),
					'label' => 'Bottom Padding',
					'name' => $module.'padding-bottom',
					'type' => 'text',
					'instructions' => 'Add padding to Module as: "10%" or "10px"',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array(
						'width' => '30%',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				*/
				array(
		            'key' => create_key($module,'background'),
		            'label' => 'Background',
		            'name' => $module . 'background',
		            'type' => 'radio',
		            'instructions' => '',
		            'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
					),
		            'wrapper' => array(
		                'width' => '50%',
		                'class' => '',
		                'id' => '',
		            ),
		            'choices' => array(
		            	'#ffffff' => 'White',
		            	'#f2f2f2' => 'Off-White'
		            ),
		            'other_choice' => 0,
		            'save_other_choice' => 0,
		            'default_value' => '',
		            'layout' => 'horizontal',
		        ),		
			);
			return $fields;
		}
	}
?>