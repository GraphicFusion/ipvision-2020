<?php
	function build_generic_content_layout(){
		$args = array(
			'background' => mason_get_sub_field('generic_content_block_background'),
			'content' => mason_get_sub_field('generic_content_block_content'),
		);
		$args['module_styles'] = [];
		if(get_sub_field('generic_content_block_background')){
			$args['module_styles']['background'] = get_sub_field('generic_content_block_background');
		}
		if(get_sub_field('generic_content_block_z-index')){
			$args['module_styles']['z-index'] = get_sub_field('generic_content_block_z-index');
		}		
		if(get_sub_field('generic_content_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('generic_content_block_margin-bottom');
		}

		return $args;
	}
?>