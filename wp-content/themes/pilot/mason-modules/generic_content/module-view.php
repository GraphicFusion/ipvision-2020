<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="container-fluid container-md container-sm">
	<div class="row">
		<div class="col-lg-12">
			<div class="gc-content">
				<?php if(array_key_exists('title', $args)) : ?>
					<h1><?php echo $args['title']; ?></h1>
				<?php endif; ?>
				<?php echo $args['content']; ?>
			</div>
		</div>
	</div>
</div>