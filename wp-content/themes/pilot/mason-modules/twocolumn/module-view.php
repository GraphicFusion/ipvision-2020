<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="container-fluid container-md container-sm">
	<div class="row">
		<div class="col-lg-8 col-sm-12 right-wrapper">
			<div class="gc-content">
				<?php if($args['right_image']) : ?>
					<img src="<?php echo $args['right_image']['url']; ?>">
				<?php endif; ?>
				<?php if($args['right_content']) : ?>
					<?php echo $args['right_content']; ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="col-lg-4 col-sm-12 left-wrapper">
			<div class="navigation">
				<?php if($args['nav']['parent']) : ?>
					<h5 class="parent"><a href='<?php echo $args['nav']['parent']['permalink']; ?>'><?php echo $args['nav']['parent']['name']; ?></a></h5>
				<?php endif; ?>				
				<?php if(count($args['nav']['children'])>0) : ?>
					<div class="children">
						<ul>
						<?php foreach($args['nav']['children'] as $child) : ?>
							<li><a href='<?php echo $child['permalink']; ?>'><?php echo $child['name']; ?></a></li>
						<?php endforeach; ?>
						</ul>
					</div>
				<?php endif; ?>
			</div>
			<div class="content">
				<?php if($args['left_content']) : ?>
					<?php echo $args['left_content']; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>