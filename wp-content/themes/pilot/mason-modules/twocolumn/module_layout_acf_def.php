<?php
	global $pilot;
	// add module layout to flexible content 
	$posts = get_posts([
		'post_type'    => array( 'page' ),
 	  'post_status' => 'publish',
	  'numberposts' => -1
	]);
	$choices = [];
	foreach($posts as $post){
		$choices[$post->ID] = $post->post_title;
	}
	$name = 'twocolumn';
	$module_layout = array (
		'key' => create_key( $name, 'block'),
		'name' => 'twocolumn_block',
		'label' => 'Two Column',
		'display' => 'block',
		'sub_fields' => array (
			array (
		        'key' => create_key( $name, 'left_content'),
				'label' => 'Left Column Content',
				'name' => 'twocolumn_block_left_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '30%',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array(
				'key' => create_key( $name, 'right_image'),
				'label' => 'Right Column Image',
				'name' => 'twocolumn_block_right_image',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '100%',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array (
		        'key' => create_key( $name, 'right_content'),
				'label' => 'Content - Right Column',
				'name' => $name . '_block_right_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '70%',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array(
		        'key' => create_key( $name, 'method'),
				'label' => 'Menu Builder - Left Column',
				'name' => $name . '_block_method',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '25%',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 1,
				'ui_on_text' => 'Manual',
				'ui_off_text' => 'Automatic',
			),
			array(
		        'key' => create_key( $name, 'page'),
				'label' => 'Main Page',
				'name' => $name . '_block_page',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => create_key( $name, 'method'),
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'choices' => $choices,
				'wrapper' => array(
					'width' => '75%',
					'class' => '',
					'id' => '',
				),
				'post_type' => '',
				'taxonomy' => '',
				'allow_null' => 1,
				'multiple' => 0,
				'return_format' => 'object',
				'ui' => 0,
			),			
		),
		'min' => '',
		'max' => '',
	);
?>