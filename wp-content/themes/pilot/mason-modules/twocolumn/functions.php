<?php
	function build_twocolumn_layout(){
		$name = 'twocolumn';
		global $post;
		$args = array(
			'left_content' => mason_get_sub_field($name . '_block_left_content'),
			'right_content' => mason_get_sub_field($name . '_block_right_content'),
			'right_image' => mason_get_sub_field($name . '_block_right_image'),
		);
		$args['nav'] = [
			'children' => [],
			'parent' => []
		];
		if(mason_get_sub_field($name . '_block_method')){
			// manual selection
			$page_id = mason_get_sub_field($name . '_block_page');
			$parent = get_post($page_id);
			$args['nav']['parent'] = [
				'id' => $parent->ID,
				'name' => $parent->post_title,
				'permalink' => get_permalink($parent)

			];
			$q = array(
			    'post_type'      => 'page',
			    'posts_per_page' => -1,
			    'post_parent'    => $page_id,
			    'order'          => 'ASC',
			    'orderby'        => 'menu_order'
			 );
			$children = new WP_Query( $q );
			if(count($children->posts)>0){
				foreach($children->posts as $child){
					$args['nav']['children'][] = [
						'id' => $child->ID,
						'name' => $child->post_title,
						'permalink' => get_permalink($child)
					];
				}
			}
		}
		else{
			/// check if is parent or child...
			$parent = get_post($post->ID);
			$args['nav']['parent'] = [
				'id' => $parent->ID,
				'name' => $parent->post_title,
				'permalink' => get_permalink($parent)

			];
			if( count( get_posts( array('post_parent' => $post->ID, 'post_type' => $post->post_type) ) ) ){

				$q = array(
				    'post_type'      => 'page',
				    'posts_per_page' => -1,
				    'post_parent'    => $parent->ID,
				    'order'          => 'ASC',
				    'orderby'        => 'menu_order'
				 );
				$children = new WP_Query( $q );
				if(count($children->posts)>0){
					foreach($children->posts as $child){
						$args['nav']['children'][] = [
							'id' => $child->ID,
							'name' => $child->post_title,
							'permalink' => get_permalink($child)
						];
					}
				}
			}
		}
		return $args;
	}
?>