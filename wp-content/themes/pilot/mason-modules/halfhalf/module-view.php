<?php
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
	$block_id = $args['acf_incr'];
	$btn_class = '';
	if( 'dark' == @$args['background'] ){
		$btn_class ='dark-bg';
	}
?>
	<div class='slides-section container-fluid bg-<?php echo @$args['background']; ?>'>
		<div class="feature-wrapper row">
			<?php if( 'right' != $args['position']) : ?>
				<div class='half-half slick-slider col-lg-6 image-<?php echo $args['position']; ?>' id="slick-slider-<?php echo $block_id; ?>">
					<img class='slides-block-image' src='<?php echo $args['image']['url']; ?>'>
		  		</div>
	  		<?php endif; ?>
	  		<div class="caption-wrapper col-lg-6 image-<?php echo $args['position']; ?> <?php echo @$args['background']; ?>">
				<div class="caption" >
					<div class="caption-inner-wrapper" >
						<?php echo $args['content']; ?>
						<?php if(isset($args['button']) && is_array($args['button']) && array_key_exists('url',$args['button'])) : ?>
							<a class="site-button <?php echo $btn_class; ?>" href='<?php echo $args['button']['url']; ?>'><?php echo $args['button']['title']; ?></a>
						<?php endif; ?>
						<?php if(isset($args['youtube']) && $args['youtube']) : ?>
							<a class="video-btn " href="<?php echo $args['youtube']; ?>">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                    <path d="M20,0C9,0,0,9,0,20s9,20,20,20c11,0,20-9,20-20S31,0,20,0z M15.4,30V10l13.3,10L15.4,30z" />
                                </svg>
                            </a>							
						<?php endif; ?>
					</div>
				</div>
		  	</div><!--/caption-wrpper-->
			<?php if( 'right' == $args['position']) : ?>
				<div class='half-half slick-slider col-lg-6 image-<?php echo $args['position']; ?>' id="slick-slider-<?php echo $block_id; ?>">
					<img class='slides-block-image' src='<?php echo $args['image']['url']; ?>'>
		  		</div>
	  		<?php endif; ?>		  	
		</div><!--/feature-wrapper-->
	</div><!--/slides-section-->
	<script>
	</script>
