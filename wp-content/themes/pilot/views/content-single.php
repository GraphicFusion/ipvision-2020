<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div style='' class='block-generic_content module'>
			<div class='layout-content'>
				<div class="container-fluid container-md container-sm">
					<div class="row">
						<div class="col-lg-12">
							<div class="gc-content">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo do_shortcode('[mason_build_blocks container=content]');?>
	</div><!-- .entry-content -->
</article>