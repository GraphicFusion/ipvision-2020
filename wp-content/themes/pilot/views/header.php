<header id="masthead" class="site-header">
    <?php echo do_shortcode('[mason_build_blocks container=menu is_option="1"]'); ?>
</header>
<?php if( get_field('add_header') || is_single()) : ?>
    <div id="optional-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>