<?php
/* 
 * Options Page
 * Add ACF items to an options page 
 * */

if( function_exists('acf_add_options_page') ) {
 
    $option_page = acf_add_options_page(array(
        'page_title'    => 'Footer',
        'menu_title'    => 'Footer',
        'menu_slug'     => 'footer',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
    $option_page = acf_add_options_page(array(
        'page_title'    => 'Main Menu',
        'menu_title'    => 'Main Menu',
        'menu_slug'     => 'header',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
    $option_page = acf_add_options_page(array(
        'page_title'    => 'Theme Settings',
        'menu_title'    => 'General Theme Settings',
        'menu_slug'     => 'styling',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
 
}
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
    'key' => 'group_5ea1da935129b',
    'title' => 'Settings',
    'fields' => array(
        array(
            'key' => 'field_5ea99f885ed1d',
            'label' => 'Snippets in Header',
            'name' => 'snippets',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
            'delay' => 0,
        ),        
        array(
            'key' => 'field_532w87rfch23ex',
            'label' => 'Snippet after Body  - Google Tag Manager ',
            'name' => 'snippet_after_body',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
            'delay' => 0,
        ),        
        array(
            'key' => 'field_DSHGWREWIDZN324^%23ex',
            'label' => 'Snippet at end of Body  - Google Tag Manager ',
            'name' => 'snippet_end_body',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
            'delay' => 0,
        ),        
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'styling',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

endif;
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
    'key' => 'group_5e6f9e1a3f491',
    'title' => 'Page Options',
    'fields' => array(
        array(
            'key' => 'field_5e6f9e279c816',
            'label' => 'Add Header with Title',
            'name' => 'add_header',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'page',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'post',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'acf_after_title',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

endif;
?>